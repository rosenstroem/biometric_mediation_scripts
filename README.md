# biometric_mediation_scripts

A README file written 23st Aug 2018.

This downloaded folder provides the essential R-language functions for the models estimated in the paper:
Genetically informative mediation modeling applied to stressors and personality-disorder traits in etiology 
of alcohol use disorder. Rosenström T, Czajkowski OT, Ystrom E, Krueger RF, Aggen SH, Gillespie NA, 
Eilertsen E, Reichborn-Kjennerud T, Torvik FA. Behavior Genetics. http://doi.org/10.1007/s10519-018-9941-z

Please, cite the latest published version of the paper also when using these scripts or their parts.

It is easiest to familiarize oneself with the methods by running, in this folder, the in-line R command:
source("user_demo.R")

This illustrates usage of our modeling tools in a simple continuous-variate case. By then examining the code in 
the text file "user_demo.R", one should get a good impression how to use this kind of functions (specified for 
'long-format' twin data). The advanced functions, TwinMed and TwinCho (not starting with "simple"), are used 
similarly, bearing necessary exceptions. Specifically, the functions take a parameter vector 
(d, alphaX = 1, alphaM = 1, alphaY = 1),
where the pre-specified reliability values, or alphas, can be modified by the user. The obligatory argument d is
a data frame that must contain ORDINAL-valued variables (ordered factors) "X", "M", and "Y", corresponding to the
mediational hypothesis. In addition, d must contain a numerical variable "sex" that gets values 0 or 1, a variable
"dzyg" containing a value 1 for a dizygotic twin and value 0 for a monozygotic twin, and a variable "pairno" that
identifies the twin pairs by giving the same unique numeric value for both members of each twin pair.

Thank you for the interest in our work. I hope you find it useful.

Best regards,
Tom Rosenström
